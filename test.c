#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <limits.h>
#include <stdbool.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <ctype.h>
#include <unistd.h>

#include "owon.h"
#include "usb_test.h"

bool parseAndExecute(void *device_handle) {
    unsigned char * buffer;
    char* input = NULL;
    int recv,total_recv;
    int long_transfer = 0;
    int total_len;
//    #define USB_BUFFER_LEN 16777216 // No funciona, libusb retorna LIBUSB_ERROR_NO_MEM
//    #define USB_BUFFER_LEN 8388608  // Funciona, pero libusb solo devuelve hasta un poco más de 2M
//    #define USB_BUFFER_LEN 2200000
    #define USB_BUFFER_LEN 32768
    #define USB_HEADER_LEN 64
    buffer = malloc(USB_BUFFER_LEN);
    if (NULL == buffer ) {
        fprintf(stderr, "Couldn't allocate buffer. Terminating\n");
        return false;
    }
    read_history(".test_history");
    while (true) {
        input = readline("--> ");
        // comprobar EOF.
        if (NULL == input) {
            break;
        }
        if (strcmp(input, "q") == 0)
        {
            break;
        }
        if (strncmp(input, ":DATA",5) == 0) {
            long_transfer = 1;
        }
        if (owon_usb_send_string(device_handle, (unsigned char*) input,  strnlen(input , 1000 )) == 0) {
            total_recv = 0;
            int multi = 0;
            do {
                recv = owon_usb_read_response(device_handle,  buffer, USB_BUFFER_LEN);
                if (recv < 0) break;
                total_recv += recv;
                if (long_transfer) {
                    // Las respuestas a :DATA: llevan la longitud de los datos en la cabecera, puede necesitar varias lecturas
                    total_len = buffer[0] | ( (int)buffer[1] << 8 ) | ( (int)buffer[2] << 16 ) | ( (int)buffer[3] << 24 );
                    total_len += 4; // Añadir su propia longitud
                    fprintf(stderr, "Long transfer (%d) bytes\n",total_len);
                    long_transfer = 0;
                    multi = 1;
                } else 
                    if (!multi) 
                        total_len = recv; // Solo por cosmética
                int i = 0;
                do
                // printf("%s\n", buffer);
                    if  isprint(buffer[i]) 
                        printf("%c", buffer[i]);
                    else
                        printf(" 0x%02x ", buffer[i]);
                while (i++ < recv && i < 600 ); // Limitar
                printf("\n");
                usleep(1000);
            } while ((total_recv < total_len) && multi);
            printf("Recibidos %d bytes. Ultimo bloque: %d bytes (Esperados %d max)\n", total_recv, recv, total_len);
        }
        // agregar al historial
        add_history(input);
        // liberar buffer devuelto por readline
        free(input);
    }
    write_history(".test_history");
    free(buffer);
    return false;
}

int main (int argc, char **argv) {

    owon_usb_init();

    struct libusb_device_handle *device_handle = owon_usb_open();
    if (NULL == device_handle) {
        fprintf(stderr, "Unable to open device\n");
        exit(EXIT_FAILURE);
    }

    printf("Introduczca secuencias SCPI. q para salir..\n");
    parseAndExecute(device_handle);
/*
    length = owon_usb_read(device_handle, buffer);
    if (0 > length) {
        fprintf(stderr, "Error reading from device: %li\n", length);
    }
*/
    owon_usb_close(device_handle);


    return 0;
}

