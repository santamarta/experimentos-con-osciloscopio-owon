
#define OWON_USB_VENDOR_ID 0x5345
#define OWON_USB_PRODUCT_ID 0x1234

#define OWON_USB_INTERFACE 0x0
#define OWON_USB_CONFIGURATION 0x1

#define OWON_USB_ENDPOINT_IN 0x81
#define OWON_USB_ENDPOINT_OUT 0x03

// Timeout en milisegundos
#define OWON_USB_TRANSFER_TIMEOUT 2000
#define OWON_USB_BIG_TRANSFER_TIMEOUT 20000

void owon_usb_init(void);

struct libusb_device_handle *owon_usb_open();
void owon_usb_close(struct libusb_device_handle *device_handle);

int owon_usb_send_string(struct libusb_device_handle *device_handle, unsigned char *buffer,int len);
int owon_usb_read_response(struct libusb_device_handle *device_handle, unsigned char *buffer, const int maxlen);

