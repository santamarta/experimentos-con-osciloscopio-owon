#include <libusb-1.0/libusb.h>
#include "owon.h"
#include "usb_test.h"
#include <stdio.h>
#include <err.h>
#include <unistd.h> // Para usleep

void owon_usb_init() {
    int init = libusb_init(NULL);
    if (init < 0) {
        errx(1,"ERROR: Cannot Initialize libusb");  
    }
}

// Posiblemente expandirlo para manejar varios dispositivos iguales
struct libusb_device_handle *owon_usb_get_device() {
    return libusb_open_device_with_vid_pid(NULL,OWON_USB_VENDOR_ID,OWON_USB_PRODUCT_ID);
}

struct libusb_device_handle *owon_usb_open() {
    int ret;
    struct libusb_device_handle *device_handle = owon_usb_get_device();
    if (!device_handle) {
        return NULL;
    }
    ret = libusb_set_configuration(device_handle, OWON_USB_CONFIGURATION);
    if (0 > ret) {
        return NULL;
    }
    ret = libusb_claim_interface(device_handle, OWON_USB_INTERFACE);
    if (0 > ret) {
        return NULL;
    }
    return device_handle;
}

int owon_usb_send_string(struct libusb_device_handle *device_handle, unsigned char *buffer,int len) {
    int ret;
    int transferido;
    ret = libusb_bulk_transfer(
            device_handle, 
            OWON_USB_ENDPOINT_OUT, 
            buffer, 
            len, 
            &transferido,
            OWON_USB_TRANSFER_TIMEOUT);
    if (ret != 0 || len != transferido) {
        fprintf(stderr, "Error sending command to device %d (%s)\n", ret , libusb_error_name(ret));
        return OWON_ERROR_USB;
    }

    return 0; 
}


int owon_usb_read_response(struct libusb_device_handle *device_handle, unsigned char *buffer, const int maxlen) {
    int ret;
    int transferido = 0;
    ret = libusb_bulk_transfer(
            device_handle, 
            OWON_USB_ENDPOINT_IN, 
            buffer, 
            maxlen, 
            &transferido,
            OWON_USB_TRANSFER_TIMEOUT);
    if ( ret != 0 ) {
        fprintf(stderr, "Error reading response from device: %d  (%s)\n",ret, libusb_error_name(ret));
        // return OWON_ERROR_USB;
    }
    return transferido;
}

// PROYECTO: Esta función sirve para transferencias que retornan la longitud de la transferencia en los primeros 32 bits.
// Como pueden llegar resultados parciales, se sigue transfiriendo hasta llegar a la longitud total o a maxlen
int owon_usb_read_long_response(struct libusb_device_handle *device_handle, unsigned char *buffer, const int maxlen) {
    int ret;
    int last_timeout=0;
    int transferido = 0;
    int oldtran = 0;
    while (transferido < maxlen) {
        ret = libusb_bulk_transfer(
            device_handle, 
            OWON_USB_ENDPOINT_IN, 
            buffer, 
            maxlen, 
            &transferido,
            OWON_USB_TRANSFER_TIMEOUT);
        // expected_length= buffer[0] | ( (int)buffer[1] << 8 ) | ( (int)buffer[2] << 16 ) | ( (int)buffer[3] << 24 );
        buffer += transferido-oldtran;
        oldtran = transferido;
        if ( ret != 0 ) {
            fprintf(stderr, "Error reading response from device: %d  (%s)\n",ret, libusb_error_name(ret));
            if (ret == LIBUSB_ERROR_TIMEOUT) { // Posible transferencia parcial, reintentar
                if (last_timeout++ < OWON_TRANSFER_MAX_RETRIES ) {
                    usleep(10000);
                    continue;
                } else
                    return OWON_ERROR_USB;
            } else
                return OWON_ERROR_USB;
        }
    }
    return transferido;
}

void owon_usb_close(struct libusb_device_handle *device_handle) {
    libusb_release_interface(device_handle, OWON_USB_INTERFACE);
    libusb_close(device_handle);
}

