CC = gcc
CFLAGS = -Wall -g# -O2
LDFLAGS = -L.
BINARIES = test
AR = ar
ARFLAGS = rcs

all: $(BINARIES)

test: test.o usb_test.o
	$(CC) $(CFLAGS) -o test test.o usb_test.o -lusb-1.0 -lreadline

test.o: owon.h usb_test.h usb_test.c test.c
	$(CC) $(CFLAGS) -c test.c -lreadline

usb_test.o: owon.h usb_test.h usb_test.c
	$(CC) $(CFLAGS) -c usb_test.c

test-net: test-net.o net_test.o
	$(CC) $(CFLAGS) -o test-net test-net.o net_test.o -lreadline

test-net.o: owon.h net_test.h net_test.c test-net.c
	$(CC) $(CFLAGS) -c test-net.c -lreadline

net_test.o: owon.h net_test.h net_test.c
	$(CC) $(CFLAGS) -c net_test.c

clean:
	rm -f *.o *.a *.so $(BINARIES)
